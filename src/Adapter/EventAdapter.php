<?php

namespace PlayGiga\EventUtility\Adapter;

abstract class EventAdapter
{
    abstract public function configure($config): bool;
    abstract public function addMessage(String $message): bool;
    abstract public function flush(): array;
}