<?php

namespace PlayGiga\EventUtility\Adapter;


class KakfaAdapter extends EventAdapter
{
    private static $instance;

    private $hosts;
    private $timeout;
    private $topic;
    private $partition;
    /**
     * @var \Kafka\Produce
     */
    private $producer;

    /**
     * Singleton
     *
     * @return KakfaAdapter
     */
    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * Configure internal class variables
     * Must return true when config is Ok
     *
     * @param $config
     * @return bool
     */
    public function configure($config): bool
    {
        $this->hosts = $config['hosts'];
        $this->timeout = $config['timeout'];
        $this->topic = $config['topic'];
        $this->partition = $config['partition'];
        $this->producer = \Kafka\Produce::getInstance($this->hosts, $this->timeout);
        $this->producer->setRequireAck(-1);
        return true;
    }


    /**
     * Adds message to client internal channel
     *
     * @param String $message String with message
     * @return bool
     */
    public function addMessage(String $message): bool
    {
        $this->producer->setMessages($this->topic, $this->partition, array($message));
        return true;
    }

    /**
     * Send every message added tp Kafka
     *
     * errorCode - Kafka response code
     * message - Error code literal description
     * time - Transaction time
     *
     * @return array
     */
    public function flush(): array
    {
        $retVal = -1;  //-1 general error code
        $currentTime = round(microtime(true) * 1000);
        try {
            $result = $this->producer->send();
        } catch (\Exception $ex) {
            return array('errorCode' => $retVal, 'message' => $ex->getMessage(), 'time' => round(microtime(true) * 1000) - $currentTime);
        }
        return array('errorCode' => $result[$this->topic][0]['errCode'], 'message' => 'Messages were processed', 'time' => round(microtime(true) * 1000) - $currentTime);
    }

    /**
     * Set Kafka topic
     *
     * @param $topic
     */
    public function setTopic($topic)
    {
        $this->topic = $topic;
    }

    /**
     * Set Kafka partition
     *
     * @param $partition
     */
    public function setPartition($partition)
    {
        $this->partition = $partition;
    }
}