<?php

namespace PlayGiga\EventUtility\Type;

class ObjectType
{
    /**
     * Event name, required
     *
     * @var String
     */
    private $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @return String
     */
    public function getName(): String
    {
        return $this->name;
    }

    /**
     * @param String $name
     */
    public function setName(String $name)
    {
        $this->name = $name;
    }


}