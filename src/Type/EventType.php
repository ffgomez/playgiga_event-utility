<?php

namespace PlayGiga\EventUtility\Type;

class EventType
{
    /**
     * Event name, required
     *
     * @var String|null
     */
    private $name;

    /**
     * Description, required
     *
     * @var String|null
     */
    private $description;

    /**
     * Array objects type, required
     */
    private $objectsType;


    public function __construct($name, $description = null, $objectsType = null)
    {
        $this->name = $name;
        $this->description = $description;
        $this->objectsType = $objectsType;
    }

    /**
     * @return String
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param String $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return String
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param String $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return null
     */
    public function getObjectsType()
    {
        return $this->objectsType;
    }

    /**
     * @param null $objectsType
     */
    public function setObjectsType($objectsType)
    {
        $this->objectsType = $objectsType;
    }


}