<?php

namespace PlayGiga\EventUtility\Component;

use mysql_xdevapi\Exception;
use PlayGiga\EventUtility\Adapter\EventAdapter;
use PlayGiga\EventUtility\Model\Event;
use PlayGiga\EventUtility\Model\Object;
use PlayGiga\EventUtility\Model\Origin;
use PlayGiga\EventUtility\Type\EventType;

/**
 * Class ComponentNotifier
 *
 * This class is used as factory multi singleton for configure and generate the component notifier
 *
 * @package PlayGiga\EventUtility\Component
 */
abstract class ComponentNotifier
{
    /**
     * Vault of static instance only one per type
     *
     * @var array
     */
    private static $_instances = array();

    /**
     * Vault of static configs of each ComponentNotifier abstrayed
     * only one per type
     *
     * @var array
     */
    private static $_configs = array();

    /**
     * Multi Singleton
     *
     * @return ComponentNotifier
     */
    public static function getInstance()
    {
        $class = get_called_class();
        if (!isset(self::$_configs[$class]['event_type_class'])) throw new \Exception('Configure ComponentEventType ComponentNotifier before use it');
        if (!isset(self::$_configs[$class]['object_type_class'])) throw new \Exception('Configure ComponentObjectType ComponentNotifier before use it');
        if (!isset(self::$_configs[$class]['origin'])) throw new \Exception('Configure Origin ComponentNotifier before use it');
        if (!isset(self::$_configs[$class]['adapter'])) throw new \Exception('Configure Adapter ComponentNotifier before use it');
        if (!isset(self::$_configs[$class]['config'])) throw new \Exception('Configure Config ComponentNotifier before use it');

        if (!isset(self::$_instances[$class])) {
            self::$_instances[$class] = new $class(self::$_configs[$class]['origin'], self::$_configs[$class]['adapter'], self::$_configs[$class]['config']);
        }
        return self::$_instances[$class];
    }

    /**
     * Get JSON human readable information about types and objects of each ComponentNotifier
     *
     * @return false|string
     */
    public static function getInfo()
    {
        $class_called = get_called_class();
        $class = new \ReflectionClass(self::$_configs[$class_called]['event_type_class']);
        $staticProperties = $class->getStaticProperties();
        $data = array();
        foreach ($staticProperties as $propertyName => $value) {
            $objects = array();
            if (self::$_configs[$class_called]['event_type_class']::${$propertyName}->getObjectsType()) {
                foreach (self::$_configs[$class_called]['event_type_class']::${$propertyName}->getObjectsType() as $propiedad => $valor) {
                    $objects[] = $valor->getName();
                }
            }
            $data[$propertyName] = array(
                'Description' => $value->getDescription(),
                'Objects' => $objects
            );
        }
        try {
            return json_encode($data, JSON_PRETTY_PRINT);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Configure ComponentNotifier before instance
     *
     * @param $eventTypeClassName String ClassName
     * @param $ComponentObjectTypeClassName String ClassName
     * @param Origin $origin Origin data
     * @param EventAdapter $adapter Event adapter for sending data
     * @param $config array Array config of event adapter
     */
    public static function configure($eventTypeClassName, $ComponentObjectTypeClassName, Origin $origin, EventAdapter $adapter, $config)
    {
        /**
         * @var $ComponentObjectTypeClassName ComponentObjectType
         */
        $ComponentObjectTypeClassName::define();
        /**
         * @var $eventTypeClassName ComponentEventType
         */
        $eventTypeClassName::define();

        $class = get_called_class();
        self::$_configs[$class] = array(
            'event_type_class' => $eventTypeClassName,
            'object_type_class' => $ComponentObjectTypeClassName,
            'origin' => $origin,
            'adapter' => $adapter,
            'config' => $config
        );
    }

    /**
     * ComponentNotifier internal constructor
     *
     * @param $origin Origin
     * @param $adapter EventAdapter
     * @param $config array
     */
    private function __construct(Origin $origin, EventAdapter $adapter, $config)
    {
        $this->origin = $origin;
        $this->adapter = $adapter;
        $this->setAdapter($adapter, $config);
    }

    /**
     * Object with data origin information
     *
     * @var Origin
     */
    private $origin = null;

    /**
     * Adaptar abstraction for add events and send
     *
     * @var EventAdapter
     */
    private $adapter = null;

    /**
     * Event Queue
     *
     * @var
     */
    private $queueEvents = array();


    abstract public function notifyEvent(EventType $envenType, ...$eventObjects);


    /**
     * Check schema
     *
     * @param $eventType
     * @param $eventObjects
     * @return bool
     * @throws \ReflectionException
     */
    private function checkEventData($eventType, $eventObjects)
    {
        $class_called = get_called_class();
        $class = new \ReflectionClass(self::$_configs[$class_called]['event_type_class']);
        $staticProperties = $class->getStaticProperties();
        $data = array();
        foreach ($staticProperties as $propertyName => $value) {
            $objects = array();
            if (self::$_configs[$class_called]['event_type_class']::${$propertyName}->getObjectsType()) {
                foreach (self::$_configs[$class_called]['event_type_class']::${$propertyName}->getObjectsType() as $propiedad => $valor) {
                    $objects[] = $valor->getName();
                }
            }
            $data[$propertyName] = $objects;
        }

        // Check if type is in schema types
        if (!array_key_exists($eventType->getName(), $data)) throw  new \Exception('Type \'' . $eventType->getName() . '\' is not support in ' . self::$_configs[$class_called]['event_type_class'] . ' schema.');
        // Check if objects is in objects schema types
        foreach ($eventObjects as $object) {
            if (!in_array($object->getType()->getName(), $data[$eventType->getName()])) throw  new \Exception('Object type \'' . $eventType->getName() . '\' is not support in ' . $eventType->getName() . ' schema.');
        }
        return true;
    }

    /**
     * Add event Type and Object to queue before prepare and sending
     *
     * @param $eventType
     * @param $eventObjects
     * @return bool
     * @throws \ReflectionException
     */
    protected function addEvent($eventType, $eventObjects)
    {
        if ($this->checkEventData($eventType, $eventObjects)) {
            $this->queueEvents[] = array(
                'type' => $eventType,
                'eventObjects' => $eventObjects
            );
            return true;
        } else return false;
    }


    /**
     * Set origin data
     *
     * @param Origin $origin
     */
    public function setOrigin(Origin $origin)
    {
        $this->origin = $origin;
    }


    /**
     * Set adapter for sending data
     *
     * @param EventAdapter $adapter
     * @param $config
     */
    public function setAdapter(EventAdapter $adapter, $config)
    {
        $this->adapter = $adapter::getInstance();
        $this->adapter->configure($config);
    }

    /**
     * Build, parsing data and serialize
     *
     * @param $eventObject
     * @return false|string
     */
    private function serializeEventObjectToMessage($eventObject)
    {
        $objects = array();
        foreach ($eventObject['eventObjects'] as $object) {
            $objects[] = new Object(
                $object->getValue()->getId(),
                $object->getValue()->getType(),
                $object->getValue()->getDescription()
            );
        }
        $event = new Event(
            $eventObject['type']->getName(),
            microtime(),
            $eventObject['type']->getDescription(),
            $this->origin,
            $objects
        );
        return json_encode($event, JSON_PRETTY_PRINT);
    }

    /**
     * Fetch queue and add string messages serialized and send using adapter
     *
     * @return array
     */
    public function flush()
    {
        foreach ($this->queueEvents as $event) {
            $this->adapter->addMessage($this->serializeEventObjectToMessage($event));
        }
        $this->queueEvents = [];
        return $this->adapter->flush();
    }
}