<?php

namespace PlayGiga\EventUtility\Component;

abstract class ComponentEventType
{
    static abstract function define();

    private $eventType;

    public function __construct($eventType)
    {
        $this->eventType = $eventType;
    }

    public function getType()
    {
        return $this->eventType->getName();
    }

    public function getDescription()
    {
        return $this->eventType->getDescription();
    }
}