<?php

namespace PlayGiga\EventUtility\Component;

use PlayGiga\EventUtility\Model\Object;

abstract class ComponentObjectType
{
    static abstract function define();

    private $type;

    private $value;

    public function __construct($objectType, $id = null, $description = null)
    {
        $this->type = $objectType;
        $this->value = new Object($id, $objectType->getName(), $description);
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return Object
     */
    public function getValue(): Object
    {
        return $this->value;
    }

    /**
     * @param Object $value
     */
    public function setValue(Object $value)
    {
        $this->value = $value;
    }


}