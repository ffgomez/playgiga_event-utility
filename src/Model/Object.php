<?php

namespace PlayGiga\EventUtility\Model;

class Object implements \JsonSerializable
{

    /**
     * Object identifier, required
     *
     * @var String
     */
    private $id;

    /**
     * Object type, required
     *
     * @var String
     */
    private $type;

    /**
     * Object description, optional
     *
     * @var String
     */
    private $description;

    /**
     * @return String
     */
    public function getId(): String
    {
        return $this->id;
    }

    /**
     * @param String $id
     */
    public function setId(String $id)
    {
        $this->id = $id;
    }

    /**
     * @return String
     */
    public function getType(): String
    {
        return $this->type;
    }

    /**
     * @param String $type
     */
    public function setType(String $type)
    {
        $this->type = $type;
    }

    /**
     * @return String
     */
    public function getDescription(): String
    {
        return $this->description;
    }

    /**
     * @param String $description
     */
    public function setDescription(String $description)
    {
        $this->description = $description;
    }


    public function __construct($id = null, $type = null, $description = null)
    {
        $this->id = $id ?? $id ?? 'UNKNOWN';
        $this->type = $type ?? $type ?? '';
        $this->description = $description ?? null;
    }


    public function jsonSerialize()
    {
        return array_filter([
            'id' => $this->id,
            'type' => $this->type,
            'description' => $this->description
        ], function ($value) {
            return !empty($value);
        });
    }
}