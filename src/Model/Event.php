<?php

namespace PlayGiga\EventUtility\Model;

class Event implements \JsonSerializable
{
    /**
     * Event type, required
     *
     * @var String
     */
    private $type;

    /**
     * Timestamp, required
     *
     * @var Float
     */
    private $timestamp;

    /**
     * Origin
     *
     * @var Origin
     */
    private $origin;

    /**
     * Objects events
     *
     * @var Object[]
     */
    private $objects = [];

    /**
     * Object error
     *
     * @var Error
     */
    private $error;


    /**
     * Description, optional
     *
     * @var String
     */
    private $description;


    /**
     * @return String
     */
    public function getType(): String
    {
        return $this->type;
    }

    /**
     * @param String $type
     */
    public function setType(String $type)
    {
        $this->type = $type;
    }

    /**
     * @return float
     */
    public function getTimestamp(): float
    {
        return $this->timestamp;
    }

    /**
     * @param float $timestamp
     */
    public function setTimestamp(float $timestamp)
    {
        $this->timestamp = $timestamp;
    }

    /**
     * @return String
     */
    public function getDescription(): String
    {
        return $this->description;
    }

    /**
     * @param String $description
     */
    public function setDescription(String $description)
    {
        $this->description = $description;
    }

    /**
     * @return Object[]
     */
    public function getObjects(): array
    {
        return $this->objects;
    }

    /**
     * @param Object[] $objects
     */
    public function setObjects(array $objects)
    {
        $this->objects = $objects;
    }

    /**
     * @return Origin
     */
    public function getOrigin(): Origin
    {
        return $this->origin;
    }

    /**
     * @param Origin $origin
     */
    public function setOrigin(array $origin)
    {
        $this->origin = $origin;
    }

    /**
     * @param Object $object
     */
    public function addObject($object)
    {
        $this->objects[] = $object;
    }

    /**
     * Event constructor, abstraction class of event
     *
     * @param null|String $type Define type event
     * @param null|Float $timestamp Timestamp in milliseconds
     * @param null|String $description Event Description
     * @param null|Origin $origin Origin Object
     * @param null|Object[] $objects Array of event objects
     * @param null|Error $error If any error
     */
    public function __construct($type = null, $timestamp = null,  $description = null, $origin = null, $objects = null, $error = null)
    {
        $this->type = $type ?? $type ?? 'UNKNOWN';
        $this->timestamp = $timestamp ?? $timestamp ?? microtime();
        $this->origin = $origin ?? $origin ?? new Origin(); // No origin by default but UNKNOWN
        $this->objects = $objects ?? $objects ?? array(); // Empty Objects by default
        $this->error = $error ?? $error ?? new Error(); // No error by default but present
        $this->description = $description ?? $description ?? null;
    }

    /**
     * Return elements array for serialize.
     * We delete empty properties like description when is NULL
     *
     * @return array|mixed
     */
    public function jsonSerialize()
    {
        return array_filter([
            'type' => $this->type,
            'timestamp' => $this->timestamp,
            'description' => $this->description,
            'origin' => $this->origin,
            'objects' => $this->objects,
            'error' => $this->error
        ],function ($value){
            return !empty($value);
        });
    }

}