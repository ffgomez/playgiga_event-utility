<?php

namespace PlayGiga\EventUtility\Model;

class Error implements \JsonSerializable
{
    /**
     * Error Code
     *
     * @var String
     */
    private $errorCode;

    /**
     * Error Text
     *
     * @var String
     */
    private $errorText;

    /**
     * If user is notified
     *
     * @var Boolean
     */
    private $notifyUser;

    /**
     * @return String
     */
    public function getErrorCode(): String
    {
        return $this->errorCode;
    }

    /**
     * @param String $errorCode
     */
    public function setErrorCode(String $errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return String
     */
    public function getErrorText(): String
    {
        return $this->errorText;
    }

    /**
     * @param String $errorText
     */
    public function setErrorText(String $errorText)
    {
        $this->errorText = $errorText;
    }

    /**
     * @return bool
     */
    public function isNotifyUser(): bool
    {
        return $this->notifyUser;
    }

    /**
     * @param bool $notifyUser
     */
    public function setNotifyUser(bool $notifyUser)
    {
        $this->notifyUser = $notifyUser;
    }

    public function __construct($errorCode = null, $errorText = null, $notifyUser = null)
    {
        $this->errorCode = $errorCode ?? $errorCode ?? '0';
        $this->errorText = $errorText ?? $errorText ?? '';
        $this->notifyUser = $notifyUser ?? $notifyUser ?? null;
    }

    public function jsonSerialize()
    {
        return [
            'errorCode' => $this->errorCode,
            'errorText' => $this->errorText,
            'notifyUser' => $this->notifyUser
        ];
    }
}