<?php

namespace PlayGiga\EventUtility\Model;

class Origin  implements \JsonSerializable
{
    /**
     * Origin identifier
     *
     * @var String
     */
    private $id;

    /**
     * Origin type
     *
     * @var String
     */
    private $type;

    /**
     * Description, optional
     *
     * @var String
     */
    private $description;

    /**
     * Version
     *
     * @var String
     */
    private $version;

    /**
     * @return String
     */
    public function getId(): String
    {
        return $this->id;
    }

    /**
     * @param String $id
     */
    public function setId(String $id)
    {
        $this->id = $id;
    }

    /**
     * @return String
     */
    public function getType(): String
    {
        return $this->type;
    }

    /**
     * @param String $type
     */
    public function setType(String $type)
    {
        $this->type = $type;
    }

    /**
     * @return String
     */
    public function getDescription(): String
    {
        return $this->description;
    }

    /**
     * @param String $description
     */
    public function setDescription(String $description)
    {
        $this->description = $description;
    }

    /**
     * @return String
     */
    public function getVersion(): String
    {
        return $this->version;
    }

    /**
     * @param String $version
     */
    public function setVersion(String $version)
    {
        $this->version = $version;
    }


    public function __construct($id = null, $type = null, $description = null, $version = null)
    {
        $this->id = $id ?? $id ?? '"0.0.0.0:8080"';
        $this->type = $type ?? $type ?? 'UNKNOWN';
        $this->description = $description ?? $description ?? null;
        $this->version = $version ?? $version ?? null;
    }
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'description' => $this->description,
            'version' => $this->version
        ];
    }

}